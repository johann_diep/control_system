%%

% implementation of the path planning algorithm RRT with no obstacles
% Johann Diep, 16 March 2016, Focusproject Scubo

clear;
clc;
%%

% creates a grid

map_width=10; % map width in meter
map_height=10; % map height in meter
map_resolution=2; % cells per meter

map=robotics.BinaryOccupancyGrid(map_width,map_height,map_resolution); % creating map
%ij=[5 6]; % location of top right corner of obstacle, x/y-coord. in meter 
%setOccupancy(map,ij,1); % creating obstacle in map
show(map);
hold on
grid on
set(gca,'XTick', 0:1/2:10, 'YTick', 0:1/map_resolution:10) % visible pattern, x- and y-axis from 0-10m, stepsize 1/resolution

%%

% variable definition

StepSize=0.1; % definition of the stepsize per interation
x_init=0; % x-coord. of initial point
y_init=0; % y-coord. of initial point
x_final=10; % x-coord. of final point
y_final=10; % y-coord. of final point
iteration=1000; % number of sampling iteration

plot(x_init,y_init,'ob','MarkerSize',10) % initialization of the start 
plot(x_final,y_final,'og','MarkerSize',10) % initialization of the goal

states_array_x=[]; % initialization of a states array for x-coordinates
states_array_y=[]; % initialization of a states array for y-coordinates

states_array_x(1:iteration+1)=x_init; % placing initial x-coord. into states array
states_array_y(1:iteration+1)=y_init; % placing initial y-coord. into states aray

distance_array=[]; % initialization of a distance array

distance_array(1:iteration+1)=0;

%%

% calculation of the nearest state

for i=1:iteration 

    x_sample=map_width*rand; % random x-coord. of sample point
    y_sample=map_height*rand; % random y-coord. of sample point
    %plot(x_sample, y_sample,'xr','MarkerSize',5) % plotting the sampled point

    for j=1:size(states_array_x,2) % using x-states as reference since both states arrays are same in size
    
        distance_array(j)=((x_sample-states_array_x(j))^2+(y_sample-states_array_y(j))^2)^(1/2); % distance from state to sample
    
    end
        
    [Lia,Locb]=ismember(min(distance_array),distance_array); % searching for the index of the smallest distance
    
    x_near=states_array_x(Locb); 
    y_near=states_array_y(Locb);
        
%    for j=1:size(states_array_x,2) % using x-states as reference since both state arrays are same in size
% 
%       distance=((x_sample-states_array_x(1))^2+(y_sample-states_array_y(1))^2)^(1/2); % initial distance between start and sample
%       distance_states=((x_sample-states_array_x(j))^2+(y_sample-states_array_y(j))^2)^(1/2); % distance from state to sample
%     
%       x_near=states_array_x(1); % defining initial state x-coord. as temporal x_near
%       y_near=states_array_y(1); % defining initial state y-coord. as temporal y_near
%     
%           if distance>distance_states
%         
%           distance=distance_states;
%           x_near=states_array_x(j); % defining temporal x_near
%           y_near=states_array_y(j); % defining temporal y_near
%         
%           end
%          
%     end

%%

    % calculation of the new state

    x_new=x_near+StepSize*cos(atan2((y_sample-y_near),(x_sample-x_near))); % x-coord. of new state
    y_new=y_near+StepSize*sin(atan2((y_sample-y_near),(x_sample-x_near))); % y-coord. of new state    

    states_array_x(i+1)=x_new; % adding x-coord. of new state into state array
    states_array_y(i+1)=y_new; % adding y-coord. of new state into state array

    plot(x_new,y_new) % plotting the new state
    hold on
    plot([x_near x_new],[y_near y_new], 'r') % connecting states
    
 %  if x_new==x_final && y_new==y_final
    if abs(x_new-x_final)<0.2 && abs(y_new-y_final) <0.2
            
        iteration;
        break
            
    end
    
end

